NaiveBayesMultinomial
Correctly Classified Instances        7880               85.3738 %
Incorrectly Classified Instances      1350               14.6262 %
Kappa statistic                          0.6906
Mean absolute error                      0.1539
Root mean squared error                  0.37  
Relative absolute error                 31.8221 %
Root relative squared error             75.2538 %
Coverage of cases (0.95 level)          88.5699 %
Mean rel. region size (0.95 level)      54.6262 %
Total Number of Instances             9230     
=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,927    0,252    0,841      0,927    0,882      0,696    0,904     0,906     no-spam
                 0,748    0,073    0,877      0,748    0,807      0,696    0,905     0,903     spam
Weighted Avg.    0,854    0,179    0,856      0,854    0,851      0,696    0,904     0,905     
=== Confusion Matrix ===

    a    b   <-- classified as
 5054  398 |    a = no-spam
  952 2826 |    b = spam

