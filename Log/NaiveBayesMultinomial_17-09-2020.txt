NaiveBayesMultinomial
Correctly Classified Instances        7869               85.1071 %
Incorrectly Classified Instances      1377               14.8929 %
Kappa statistic                          0.6847
Mean absolute error                      0.1553
Root mean squared error                  0.3719
Relative absolute error                 32.1242 %
Root relative squared error             75.6501 %
Coverage of cases (0.95 level)          88.6221 %
Mean rel. region size (0.95 level)      54.5912 %
Total Number of Instances             9246     
=== Detailed Accuracy By Class ===

                 TP Rate  FP Rate  Precision  Recall   F-Measure  MCC      ROC Area  PRC Area  Class
                 0,926    0,257    0,839      0,926    0,880      0,690    0,904     0,906     no-spam
                 0,743    0,074    0,873      0,743    0,803      0,690    0,904     0,902     spam
Weighted Avg.    0,851    0,182    0,853      0,851    0,849      0,690    0,904     0,905     
=== Confusion Matrix ===

    a    b   <-- classified as
 5059  407 |    a = no-spam
  970 2810 |    b = spam

