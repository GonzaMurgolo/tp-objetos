package frontend;

import java.io.IOException;
import java.lang.reflect.Field;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.List;

import com.jfoenix.controls.JFXButton;

import backend.TweetMiner;
import backend.TwitterFilterDto;
import javafx.animation.KeyFrame;
import javafx.animation.PauseTransition;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import twitter4j.JSONObject;
import twitter4j.TwitterException;

public class TableSelectedTweetsController extends Controller {

	@FXML private TableView<TableTweetsRow> table;
	@FXML private TableColumn<TableTweetsRow, String> tweetColumn;
	@FXML private TableColumn<TableTweetsRow, String> dateColumn;
	@FXML private TableColumn<TableTweetsRow, JFXButton> deleteColumn;
	@FXML private Pane classifyTweetsDialog = new Pane();

	private TwitterFilterDto filter;
    private TweetMiner tM = new TweetMiner();
    
    public ObservableList<TableTweetsRow> tweetsMined = FXCollections.observableArrayList();
	
	public void setUserInputs(Inputs inputs) {
		searchValue = inputs.searchValue;
		textUserValue = inputs.userNameValue;
		classifierValue = inputs.clasifValue;
		cantTweetsValue = inputs.cantValue;
		filter = inputs.filter;
		contexto = inputs.contexto;
		
		if (searchValue == TIMELINE_PUBLICO) {
			readTimelineTweets();
			return;
		}
		if (searchValue == USUARIO_PARTICULAR) {
			fetchTweetsFromUser(textUserValue, cantTweetsValue);
		}
	}
	
	private void readTimelineTweets() {
		int cantTweets = cantTweetsValue;
		try {
			readTweets(cantTweets);
		} catch (TwitterException e) {
			e.printStackTrace();
		}
	}
    
    private void readTweets(Integer cantTweets) throws TwitterException {
		Integer cant = cantTweets;
		if (cant > 0) {
			tweetsList = emptyFilter() ? tM.mineTimelineTweets(cantTweets) : tM.mineTimelineTweetsWithFilter(cantTweets, filter);
			addTweetsToTable(tweetsList);
		}
	}
    
    private boolean emptyFilter() {
    	return filter.getKeyword() == null && filter.getFromDate() == null && filter.getToDate() == null;
    }
    
    
    private void fetchTweetsFromUser(String username, Integer cantTweets) {
		if (cantTweets > 0) {
			tweetsList = tM.getTweetsByUser(username, cantTweets, filter);
			addTweetsToTable(tweetsList);
		}
	}
    
    private void addTweetsToTable(List<JSONObject> tweets) {
    	for (JSONObject s: tweets) {
    		Format formatter = new SimpleDateFormat("dd-MM-yyyy");
            TableTweetsRow toAdd = createRowData((String) s.get("text"), formatter.format(s.get("createdAt")));
            tweetsMined.add(toAdd);
            
            // Set table columns styles
            tweetColumn.setCellValueFactory(new PropertyValueFactory<>("tweet"));
            dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));
            dateColumn.setStyle( "-fx-alignment: CENTER;");
            deleteColumn.setCellValueFactory(new PropertyValueFactory<>("deleteBox"));
            deleteColumn.setStyle( "-fx-alignment: CENTER;");
            setTweetColumnTextWrap();
            
            table.setItems(tweetsMined);
        }
    }
    
    private void setTweetColumnTextWrap() {
    	tweetColumn.setCellFactory(tc -> {
            TableCell<TableTweetsRow, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            text.wrappingWidthProperty().bind(tweetColumn.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell ;
        });
    }
    
    private TableTweetsRow createRowData(String s, String date) {
    	JFXButton btn = new JFXButton();
    	btn.setMaxSize(30,30);
    	btn.setGraphic(getImageView("/frontend/images/deleteIconLight.png"));
    	setDeleteButtonHoverEvent(btn);
    	
    	TableTweetsRow toAdd = new TableTweetsRow(s, date, btn);
    	setDeleteButtonClickEvent(btn, toAdd);
    	return toAdd;
    }
    
    private ImageView getImageView(String url) {
    	Image img = new Image(getClass().getResource(url).toExternalForm(), 30, 30, true, true);
    	return new ImageView(img);
    }
    
    private void setDeleteButtonHoverEvent(JFXButton btn) {
    	Tooltip t = new Tooltip("Delete tweet");
    	changeTooltipStartTiming(t);
    	btn.setTooltip(t);
    	btn.setOnMouseEntered(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	btn.setGraphic(getImageView("/frontend/images/deleteIconHover.png"));
            }
        });

    	btn.setOnMouseExited(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent t) {
            	btn.setGraphic(getImageView("/frontend/images/deleteIconLight.png"));
            }
        });
    }
    
    public static void changeTooltipStartTiming(Tooltip tooltip) {
        try {
            Field fieldBehavior = tooltip.getClass().getDeclaredField("BEHAVIOR");
            fieldBehavior.setAccessible(true);
            Object objBehavior = fieldBehavior.get(tooltip);

            Field fieldTimer = objBehavior.getClass().getDeclaredField("activationTimer");
            fieldTimer.setAccessible(true);
            Timeline objTimer = (Timeline) fieldTimer.get(objBehavior);

            objTimer.getKeyFrames().clear();
            objTimer.getKeyFrames().add(new KeyFrame(new Duration(250)));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    private void setDeleteButtonClickEvent(JFXButton btn, TableTweetsRow toAdd) {
    	btn.setOnMouseClicked(v->{
    		tweetsList.remove(tweetsMined.indexOf(toAdd));
    		this.tweetsMined.remove(toAdd);
    		TableTweetsRow selectedItem = (TableTweetsRow) table.getSelectionModel().getSelectedItem();
    	    table.getItems().remove(selectedItem);
    	});
    }
    
    @FXML
    public void backButtonClicked(MouseEvent event) throws IOException {
    	goToBack(event, "/frontend/fxml/SetUpWindow.fxml", SELECTED_TWEETS_CONT);
    }
    
    @FXML
    public void classifyButtonClicked(MouseEvent mouseEvent) throws IOException {
    	if (!tweetsMined.isEmpty()) {
    		classifyTweetsDialog.setVisible(true);
    		PauseTransition visiblePause = new PauseTransition(Duration.seconds(1));
        	visiblePause.setOnFinished(
    			event -> {
    				try {
    					openNextWindow(mouseEvent);
					} catch (IOException e) {
						e.printStackTrace();
					}
    	        }
        	);
        	visiblePause.play();
    	}
    }
    
    private void openNextWindow(MouseEvent mouseEvent) throws IOException {
    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/frontend/fxml/ClassifyTweetsWindow.fxml"));
        Parent classifyTweetsWindowParent = (Parent) loader.load();

        ClassifyTweetsController classifyTweetsController = loader.getController();
        Inputs inputsFromSetup = new Inputs(searchValue, classifierValue, cantTweetsValue, textUserValue, null, contexto);
        classifyTweetsController.setNextWindowValues(tweetsList, inputsFromSetup);
        
        Scene classifyTweetsWindowScene = new Scene(classifyTweetsWindowParent);
        Stage window2 = (Stage) ((Node) mouseEvent.getSource()).getScene().getWindow();
        window2.setScene(classifyTweetsWindowScene);
        window2.show();
    }

    public void setOldTweetsList(List<JSONObject> tweetsList, Inputs inputs) {
    	searchValue = inputs.searchValue;
		textUserValue = inputs.userNameValue;
		classifierValue = inputs.clasifValue;
		cantTweetsValue = inputs.cantValue;
		this.tweetsList = tweetsList;
		contexto = inputs.contexto;
    	addTweetsToTable(tweetsList);
    }
	
}
