package frontend;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		Parent root = FXMLLoader.load(getClass().getResource("/frontend/fxml/LogInWindow.fxml"));
		Scene scene = new Scene(root, 900, 650);
		scene.getStylesheets().add(getClass().getResource("/frontend/css/LogInStyle.css").toExternalForm());
		primaryStage.setResizable(false);
		primaryStage.setTitle("Trabajo final Programación orientada a objetos");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	public static void main(String[] args) {
		launch(args);
	}
}
