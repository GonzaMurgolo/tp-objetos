package frontend;

import java.io.IOException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDatePicker;
import com.jfoenix.controls.JFXTextField;

import backend.ClasificadorJ48;
import backend.ClasificadorNaivesBayesMN;
import backend.Context;
import backend.TwitterFilterDto;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.util.Duration;

public class SetUpController extends Controller implements Initializable {

	@FXML private ChoiceBox classifierSelector = new ChoiceBox();
	@FXML private ChoiceBox searchMethodSelector = new ChoiceBox();
	@FXML private ChoiceBox cantTweetsSelector = new ChoiceBox();
	@FXML private JFXTextField textFieldUserName = new JFXTextField();
    @FXML private ImageView backButton = new ImageView();
    @FXML private JFXButton confirmButton;
    @FXML private Label requiredUsername = new Label();
    @FXML private JFXTextField textFilter = new JFXTextField();
    @FXML private JFXDatePicker fromDateFilter = new JFXDatePicker();
    @FXML private JFXDatePicker toDateFilter = new JFXDatePicker();
    @FXML private Pane incompleteFieldsDialog = new Pane();
    @FXML private Pane loadingTweetsDialog = new Pane();
    @FXML private Pane wrongDatesDialog = new Pane();
    
    private static final Integer[] CANT_TWEETS = {10, 20, 30, 40, 50};
    
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        setClassifierSelector();
        setSearchMethodSelector();
        setCantTweetsSelector();
        
        setBackButtonClickEvent();
        
    }
    
    private void setClassifierSelector() {
    	ArrayList<Object> arrayList = new ArrayList<Object>();
        arrayList.add(NBM);
        arrayList.add(new Separator());
        arrayList.add(J48);
        classifierSelector.setItems(FXCollections.observableArrayList(arrayList));
    }
    
    private void setCantTweetsSelector() {
    	ArrayList<Object> arrayList = new ArrayList<Object>();
    	for (Integer cantidad: CANT_TWEETS) {
    		arrayList.add(cantidad);
    		arrayList.add(new Separator());
    	}
    	cantTweetsSelector.setItems(FXCollections.observableArrayList(arrayList));
    }
    
    private void setSearchMethodSelector() {
    	ArrayList<Object> arrayList = new ArrayList<Object>();
        arrayList.add(TIMELINE_PUBLICO);
        arrayList.add(new Separator());
        arrayList.add(USUARIO_PARTICULAR);
        searchMethodSelector.setItems(FXCollections.observableArrayList(arrayList));
        
        searchMethodSelector.getSelectionModel().selectedIndexProperty()
        .addListener(new ChangeListener<Number>() {
          public void changed(ObservableValue<? extends Number> ov, Number value, Number new_value) {
            if(new_value == (Number) (arrayList.indexOf(USUARIO_PARTICULAR))) {
            	textFieldUserName.setDisable(false);
            	textFieldUserName.setEditable(true);
            	textFieldUserName.setOpacity(0.8);
            	requiredUsername.setVisible(true);
            } else {
            	textFieldUserName.setText("");
            	textFieldUserName.setDisable(true);
            	textFieldUserName.setEditable(false);
            	textFieldUserName.setOpacity(0.4);
            	requiredUsername.setVisible(false);
            }
          }
        });
    }
    
    private void setBackButtonClickEvent() {
    	backButton.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
					backButtonClicked(event);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
            }
        });
    }
    
    @FXML
    public void backButtonClicked(MouseEvent event) throws IOException {
        goToBack(event, "/frontend/fxml/LogInWindow.fxml", SETUP_CONT);
    }
    
    @FXML
    public void confirmButtonClicked(ActionEvent actionEvent) throws IOException {
    	
    	if (validateInputValues()) {
    		
    		if (validateDates()) {    			
    			loadingTweetsDialog.setVisible(true);
    			PauseTransition visiblePause = new PauseTransition(Duration.seconds(1));
    			visiblePause.setOnFinished(
    					event -> {
    						try {
								openNextWindow(actionEvent);
							} catch (IOException e) {
								e.printStackTrace();
							}
    					}
    					);
    			visiblePause.play();
    		} else {
    			showDialog(wrongDatesDialog);
    		}
    		
    	} else {
    		showDialog(incompleteFieldsDialog);
    	}
    	
    }
    
    private boolean validateInputValues() {
    	return 	classifierSelector.getValue() != null && cantTweetsSelector.getValue() != null && searchMethodSelector.getValue() != null &&
			((searchMethodSelector.getValue() == USUARIO_PARTICULAR && textFieldUserName.getText() != "") || searchMethodSelector.getValue() == TIMELINE_PUBLICO);
    }
    
    private boolean validateDates() {
    	LocalDate todayDate = java.time.LocalDate.now().plusDays(1);
    	if (fromDateFilter.getValue() != null && toDateFilter.getValue() == null) {
    		return fromDateFilter.getValue().isBefore(todayDate);
    	}
    	if (fromDateFilter.getValue() == null && toDateFilter.getValue() != null) {
    		return toDateFilter.getValue().isBefore(todayDate);
    	}
    	if (fromDateFilter.getValue() != null && toDateFilter.getValue() != null) {
    		return 	fromDateFilter.getValue().isBefore(toDateFilter.getValue()) &&
    				fromDateFilter.getValue().isBefore(todayDate) && toDateFilter.getValue().isBefore(todayDate);
    	}
    	return true;
    }
    
    private void openNextWindow(ActionEvent event) throws IOException {
    	Context contexto = null;
		try {
			contexto = new Context(classifierSelector.getValue() == J48 ? new ClasificadorJ48() : new ClasificadorNaivesBayesMN());
		} catch (Exception e) {
			e.printStackTrace();
		}
    	Inputs inputsFromSetup = new Inputs(searchMethodSelector.getValue().toString(), classifierSelector.getValue().toString(),
    			(Integer) cantTweetsSelector.getValue(), textFieldUserName.getText().toString(), getFilter(), contexto);
    	
    	openNextWindow(event, "/frontend/fxml/TableSelectedTweetsWindow.fxml", SETUP_CONT, inputsFromSetup);
    }
    
    public TwitterFilterDto getFilter() {
    	TwitterFilterDto filter = new TwitterFilterDto();
    	Date fromDate = null; Date toDate = null;
    	try {
			fromDate = fromDateFilter.getValue() != null ? getFormattedDate(fromDateFilter.getValue()) : null;
			toDate = toDateFilter.getValue() != null ? getFormattedDate(toDateFilter.getValue()) : null;
		} catch (ParseException e) {
		}
    	filter.setFromDate(fromDate);
    	filter.setToDate(toDate);
    	filter.setKeyword(textFilter.getText());
    	return filter;
    }
    
    private Date getFormattedDate(LocalDate date) throws ParseException {
    	StringBuilder strBuilder = new StringBuilder();
    	strBuilder.append(Integer.toString(date.getYear()) + "-");
    	strBuilder.append(Integer.toString(date.getMonthValue()) + "-");
    	strBuilder.append(Integer.toString(date.getDayOfMonth()));
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(strBuilder.toString());
	}
    
    public void setOldInputValues(Inputs i) {
    	searchMethodSelector.setValue(i.searchValue);
    	classifierSelector.setValue(i.clasifValue);
    	cantTweetsSelector.setValue(i.cantValue);
    	textFieldUserName.setText(i.userNameValue);
    }
    
    @FXML
    public void resetInputsButtonClicked(ActionEvent actionEvent) {
    	searchMethodSelector.setValue(null);
    	classifierSelector.setValue(null);
    	cantTweetsSelector.setValue(null);
    	textFieldUserName.setText(null);
    	textFilter.setText(null);
    	fromDateFilter.setValue(null);
    	toDateFilter.setValue(null);
    }

}
