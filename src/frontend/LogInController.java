package frontend;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import java.io.IOException;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;

public class LogInController extends Controller {
	
	@FXML private JFXButton btnLogIn;
    @FXML private JFXPasswordField pass;
    @FXML private JFXTextField user;
    @FXML private AnchorPane anchorPane;
    @FXML private GridPane gridPane;
    @FXML private JFXButton startButton;
    
    public static final String LOGIN_CONT = "LogInController";
    
    @FXML
    public void startButtonClicked(ActionEvent event) throws IOException {
    	openNextWindow(event, "/frontend/fxml/SetUpWindow.fxml", LOGIN_CONT, null);
    }
	
}
