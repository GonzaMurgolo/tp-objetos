package frontend;

import java.io.IOException;
import java.util.List;

import backend.Context;
import javafx.animation.PauseTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;
import javafx.util.Duration;
import twitter4j.JSONObject;

public abstract class Controller {

	protected static final String NBM = "NAIVE BAYES MULTINOMIAL";
	protected static final String J48 = "J48";
	protected static final String TIMELINE_PUBLICO = "TIMELINE P�BLICO";
	protected static final String USUARIO_PARTICULAR = "USUARIO PARTICULAR";
	protected static final String SETUP_CONT = "SetUpController";
	protected static final String SELECTED_TWEETS_CONT = "TableSelectedTweetsController";
	protected static final String CLASSIFY_CONT = "ClassifyTweetsController";
	
	protected String searchValue;
	protected String textUserValue;
	protected String classifierValue;
	protected Integer cantTweetsValue;
	protected List<JSONObject> tweetsList;
	protected Context contexto;
	
	public void goToBack(MouseEvent event, String finalWindow, String sourceController) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(finalWindow));
        Parent windowParent = (Parent) loader.load();

        switch(sourceController) {
	        case SELECTED_TWEETS_CONT:
	        	SetUpController setUpWindowController = loader.getController();
	        	Inputs inputsFromTable = new Inputs(searchValue, classifierValue, cantTweetsValue, textUserValue, null, contexto);
	        	setUpWindowController.setOldInputValues(inputsFromTable);
	        	break;
	        case CLASSIFY_CONT:
	        	TableSelectedTweetsController tableSelectedTweetsController = loader.getController();
	        	Inputs inputsFromClassif = new Inputs(searchValue, classifierValue, cantTweetsValue, textUserValue, null, contexto);
	            tableSelectedTweetsController.setOldTweetsList(tweetsList, inputsFromClassif);
	            break;
        }
        
        Scene windowScene = new Scene(windowParent);
        Stage window2 = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window2.setScene(windowScene);
        window2.show();
	}
	
	public void openNextWindow(ActionEvent event, String finalWindow, String controller, Inputs inputs) throws IOException {
		FXMLLoader loader = new FXMLLoader(getClass().getResource(finalWindow));
        Parent windowParent = (Parent) loader.load();

        switch(controller) {
	        case SETUP_CONT:
	        	TableSelectedTweetsController tableSelectedTweetsWindow = loader.getController();
	        	tableSelectedTweetsWindow.setUserInputs(inputs);
	        	break;
        }
        
        Scene windowScene = new Scene(windowParent);
        Stage window2 = (Stage) ((Node) event.getSource()).getScene().getWindow();
        window2.setScene(windowScene);
        window2.show();
	}
	
	public void showDialog(Pane dialog) {
    	dialog.setVisible(true);
    	PauseTransition visiblePause = new PauseTransition(
    	        Duration.seconds(3)
    	);
    	visiblePause.setOnFinished(
    	        event -> dialog.setVisible(false)
    	);
    	visiblePause.play();
    }
	
}
