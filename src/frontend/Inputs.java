package frontend;

import backend.Context;
import backend.TwitterFilterDto;

public class Inputs {

	public String searchValue;
	public String clasifValue;
	public Integer cantValue;
	public String userNameValue;
	public TwitterFilterDto filter;
	public Context contexto;
	
	public Inputs(String sv, String cv, Integer cantv, String uv, TwitterFilterDto filter, Context contexto) {
		this.searchValue = sv;
		this.clasifValue = cv;
		this.cantValue = cantv;
		this.userNameValue = uv;
		this.filter = filter;
		this.contexto = contexto;
	}
	
}
