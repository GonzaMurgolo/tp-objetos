package frontend;

import com.jfoenix.controls.JFXButton;

public class TableTweetsRow {

    private String tweet;

    private String date;

	private JFXButton deleteBox;

    public TableTweetsRow(String tweet, String date, JFXButton deleteBox){
        this.tweet = tweet;
        this.deleteBox = deleteBox;
        this.date = date;
    }

    public String getTweet(){
        return this.tweet;
    }

    public void setTweet(String tweet){
        this.tweet = tweet;
    }
    
    public String getDate() {
    	return date;
    }
    
    public void setDate(String date) {
    	this.date = date;
    }

    public void setDeleteBox(JFXButton deleteBox){
        this.deleteBox = deleteBox;
    }

    public JFXButton getDeleteBox(){
        return this.deleteBox;
    }

    @Override
    public String toString(){
        return this.tweet;
    }
}
