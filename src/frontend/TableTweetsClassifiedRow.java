package frontend;

import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;

public class TableTweetsClassifiedRow {

	private CheckBox checkClasifColumn;
	
	private String tweet;

    private Label spam;

    public TableTweetsClassifiedRow(CheckBox checkClasifColumn, String tweet, Label spam){
    	this.checkClasifColumn = checkClasifColumn;
        this.tweet = tweet;
        this.spam = spam;
    }
    
    public CheckBox getCheckClasifColumn(){
        return this.checkClasifColumn;
    }

    public void setCheckClasifColumn(CheckBox checkClasifColumn){
        this.checkClasifColumn = checkClasifColumn;
    }

    public String getTweet(){
        return this.tweet;
    }

    public void setTweet(String tweet){
        this.tweet = tweet;
    }
    
    public Label getSpam(){
    	return this.spam;
    }

    public void setSpam(Label spam){
        this.spam = spam;
    }

    @Override
    public String toString(){
        return this.tweet;
    }
	
}
