package frontend;

import static backend.DatasetUtils.inferClass;
import static backend.DatasetUtils.addToDataset;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.swing.JOptionPane;

import com.jfoenix.controls.JFXButton;

import backend.ClasificadorJ48;
import backend.ClasificadorNaivesBayesMN;
import backend.Context;
import backend.DatasetClass;
import backend.NewDatasetElementDto;
import javafx.animation.PauseTransition;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.Tooltip;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.util.Duration;
import twitter4j.JSONObject;

public class ClassifyTweetsController extends Controller {
	
	@FXML private TableView<TableTweetsClassifiedRow> table;
	@FXML private TableColumn<TableTweetsClassifiedRow, String> tweetColumn;
	@FXML private TableColumn<TableTweetsClassifiedRow, CheckBox> checkClasifColumn;
	@FXML private TableColumn<TableTweetsClassifiedRow, Label> clasifColumn;
	@FXML private Label lblCantSpam;
	@FXML private Label lblCantNoSpam;
	@FXML private JFXButton statsButton = new JFXButton();
	@FXML private JFXButton addDatasetButton = new JFXButton();
	@FXML private Pane successDialog = new Pane();
	@FXML private Pane trainingModelsDialog = new Pane();
	@FXML private ImageView selectAll = new ImageView();
	
	private Integer cantSpam;
	private Integer cantNoSpam;
	private ArrayList<String> stats = new ArrayList<>();
	private String statsToShow;
	private ObservableList<TableTweetsClassifiedRow> tweetsMined = FXCollections.observableArrayList();
	
	public void setNextWindowValues(List<JSONObject> tl, Inputs i) {
		searchValue = i.searchValue;
		textUserValue = i.userNameValue;
		classifierValue = i.clasifValue;
		cantTweetsValue = i.cantValue;
		tweetsList = tl;
		contexto = i.contexto;
		cantSpam = 0;
		cantNoSpam = 0;
		setSelectAllHover();
		processTweets(tweetsList);
	}
	
	private void setSelectAllHover() {
		Tooltip.install(selectAll, new Tooltip("Seleccionar todos"));
	}
	
	 private void processTweets(List<JSONObject> tweets) {
    	if (classifierValue == J48) {
    		tweets.stream().map(tw->(String) tw.get("text")).forEach(this::classifierJ48);
    		setLabelTotalValues();
    		loadStatistics(J48);
    		return;
    	}
    	if (classifierValue == NBM) {
    		tweets.stream().map(tw->(String) tw.get("text")).forEach(this::classifierNB);
    		setLabelTotalValues();
    		loadStatistics("NaiveBayesMultinomial");
    		return;
    	}
	}
	 
	public void loadStatistics(String classifName) {
		String filename = getLastFilename(classifName);
		try {
			BufferedReader reader = new BufferedReader(new FileReader("Log/" + filename));
			String line;
			while ((line = reader.readLine()) != null) {
				stats.add(line);
			}
			reader.close();
			statsToShow = fromStatsArrayToString();
		} catch (IOException e) {
			System.out.println("No se pudo cargar el archivo: " + filename);
		}
	}
	
	private String getLastFilename(String classifName) {
		File auxFile = null;
		try {
			List<File> filesList = Files.walk(Paths.get("Log"))
			        .filter(Files::isRegularFile)
			        .map(Path::toFile)
			        .filter(file -> (file.getName().contains(classifName)))
			        .collect(Collectors.toList());
			auxFile = filesList.get(0);
			for (File file: filesList) {
				if (file.lastModified() > auxFile.lastModified()) {
					auxFile = file;
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return auxFile.getName();
	}
	
	private String fromStatsArrayToString() {
		StringBuilder builder = new StringBuilder();
		stats.set(0, stats.get(0) + System.getProperty("line.separator"));
		stats.set(10, stats.get(10) + System.getProperty("line.separator"));
		stats.set(13, "         " + stats.get(13));
		stats.set(14, "              " + stats.get(14));
		stats.set(15, "              " + stats.get(15));
		stats.set(16, stats.get(16) + System.getProperty("line.separator"));
		for (String line: stats) {
	        builder.append(line).append(System.getProperty("line.separator"));
	    }
		return builder.toString();
	}
	 
	private void setLabelTotalValues() {
		lblCantSpam.setText("Spam: " + cantSpam);
		lblCantNoSpam.setText("No spam: " + cantNoSpam);
	}
    
    private Optional<String> classifierJ48(String tweet) {
		String clasification = null;
		try {
			clasification = contexto.classify(tweet);
			incrementCounterSpam(clasification);
			addClassifiedToTable(tweet, clasification);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(clasification);
	}
	
	private Optional<String> classifierNB(String tweet) {
		String clasification= null;
		try {
			clasification = contexto.classify(tweet);
			incrementCounterSpam(clasification);
			addClassifiedToTable(tweet, clasification);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(clasification);
	}
	
	private void incrementCounterSpam(String clasification) {
		if (clasification.equals("spam")) {
			cantSpam++;
		} else {
			cantNoSpam++;
		}
	}
	
	private void addClassifiedToTable(String tweet, String classification) {
    	TableTweetsClassifiedRow toAdd = createRowData(tweet, classification);
		tweetsMined.add(toAdd);
        
        // Set table columns styles
        tweetColumn.setCellValueFactory(new PropertyValueFactory<>("tweet"));
        checkClasifColumn.setCellValueFactory(new PropertyValueFactory<>("checkClasifColumn"));
        clasifColumn.setCellValueFactory(new PropertyValueFactory<>("spam"));
        checkClasifColumn.setStyle( "-fx-alignment: CENTER;");
        clasifColumn.setStyle( "-fx-alignment: CENTER;");
        setTweetColumnTextWrap();
        
        table.setItems(tweetsMined);
    }
    
    private void setTweetColumnTextWrap() {
    	tweetColumn.setCellFactory(tc -> {
            TableCell<TableTweetsClassifiedRow, String> cell = new TableCell<>();
            Text text = new Text();
            cell.setGraphic(text);
            text.wrappingWidthProperty().bind(tweetColumn.widthProperty());
            text.textProperty().bind(cell.itemProperty());
            return cell ;
        });
    }
    
    private TableTweetsClassifiedRow createRowData(String s, String clasif) {
    	CheckBox chk = new CheckBox();
    	chk.setMaxSize(15,15);
    	Label lbl = new Label(clasif);
    	lbl.getStylesheets().add(clasif.equals(DatasetClass.SPAM.getDirValue()) ? 
    			getClass().getResource("/frontend/css/LabelSpamStyle.css").toExternalForm() :
    			getClass().getResource("/frontend/css/LabelNoSpamStyle.css").toExternalForm());
    	setLabelClickEvent(lbl, chk);
    	
    	TableTweetsClassifiedRow toAdd = new TableTweetsClassifiedRow(chk, s, lbl);
    	return toAdd;
    }
    
    private void setLabelClickEvent(Label lbl, CheckBox chk) { 
    	lbl.setOnMouseClicked((mouseEvent) -> {
            if (lbl.getText().equals(DatasetClass.SPAM.getDirValue())) {
            	lbl.setText(DatasetClass.NO_SPAM.getDirValue());
            	lbl.getStylesheets().clear();
            	lbl.getStylesheets().add(getClass().getResource("/frontend/css/LabelNoSpamStyle.css").toExternalForm());
            } else {
            	lbl.setText(DatasetClass.SPAM.getDirValue());
            	lbl.getStylesheets().clear();
            	lbl.getStylesheets().add(getClass().getResource("/frontend/css/LabelSpamStyle.css").toExternalForm());
            }
            chk.setSelected(true);
        });
    }
	
	@FXML
    public void backButtonClicked(MouseEvent event) throws IOException {
		goToBack(event, "/frontend/fxml/TableSelectedTweetsWindow.fxml", CLASSIFY_CONT);
    }
	
	@FXML
    public void homeButtonClicked(MouseEvent event) throws IOException {
        Parent setUpWindowParent = FXMLLoader.load(getClass().getResource("/frontend/fxml/SetUpWindow.fxml"));
        Scene setUpWindowScene = new Scene(setUpWindowParent);

        Stage window = (Stage) ((Node) event.getSource()).getScene().getWindow();

        window.setScene(setUpWindowScene);
        window.show();
    }
	
	@FXML
    public void statsButtonClicked(ActionEvent actionEvent) throws IOException {
    	
	    JOptionPane.showMessageDialog(null, statsToShow);
    	
    }
	
	@FXML
    public void addDatasetButtonClicked(ActionEvent actionEvent) throws IOException {
    	ArrayList<NewDatasetElementDto> elementsToAdd = new ArrayList<>();
    	for (TableTweetsClassifiedRow row: table.getItems()) {
    		if (row.getCheckClasifColumn().isSelected()) {
    			elementsToAdd.add(new NewDatasetElementDto(inferClass(row.getSpam().getText()), row.getTweet()));
    		}
    	}
    	
    	if (!elementsToAdd.isEmpty()) {
    		showDialog(trainingModelsDialog);
    		PauseTransition visiblePause = new PauseTransition(Duration.seconds(1));
        	visiblePause.setOnFinished(
    			event -> {
    				updateDataset(elementsToAdd);
    	        }
        	);
        	visiblePause.play();
    	}
    }
	
	private void updateDataset(ArrayList<NewDatasetElementDto> elementsToAdd) {
		addToDataset(elementsToAdd);
		try {
			Context contextoBayes = new Context(new ClasificadorNaivesBayesMN());
			contextoBayes.reTrainModel();
//			Context contextoJ48 = new Context(new ClasificadorJ48());
//			contextoJ48.reTrainModel();
		} catch (Exception e) {
			e.printStackTrace();
		}
		showDialog(successDialog);
	}
	
	@FXML
    public void selectAllClicked(MouseEvent event) throws IOException {
		for (TableTweetsClassifiedRow row: table.getItems()) {
			row.getCheckClasifColumn().setSelected(true);
    	}
    }
	
}
