package backend;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;
import java.util.stream.Stream;

import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.core.converters.TextDirectoryLoader;

public class DatasetUtils {
	private static final String DATA_SET_DIR = "DataSetRaw";
	private static final String TWEET_FILE_EXTENSION = ".txt";
	private static final String TWEET_ADDED_PREFIX = "tweet_added_";
	public static final String DATA_ARFF = "./Normalizado/data.arff";
	public static final Integer CLASS_INDEX = 1;
	private static final String RELATION_NAME = "spamDataSet";

	private DatasetUtils() {
	}

	public static void generateArff() {// Genera un archivo .arff a partir de las carpetas con cada categoria
		TextDirectoryLoader arffLoader = new TextDirectoryLoader();
		try {
			System.out.println("Going to normalize datasets");
			File directory = new File(DATA_SET_DIR);
			arffLoader.setDirectory(directory);
			Instances dataSet = arffLoader.getDataSet();
			dataSet.setRelationName(RELATION_NAME);
			ArffSaver saver = new ArffSaver();
			saver.setInstances(dataSet);
			saver.setFile(new File(DATA_ARFF));
			saver.writeBatch();
			System.out.println("Finished. Normalized " + dataSet.size() + " instances");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void addToDataset(List<NewDatasetElementDto> newDatasetElements) {
		Integer elementToAddId = getLastIdAdded();
		for (NewDatasetElementDto newDatasetElementDto : newDatasetElements) {
			elementToAddId++;
			createFile(newDatasetElementDto, elementToAddId);
		}
	}

	private static void createFile(NewDatasetElementDto ndeDto, Integer newId)  {

		File d = new File(DATA_SET_DIR + "\\" + ndeDto.getClassification().getDirValue() + "\\" + TWEET_ADDED_PREFIX + newId
				+ TWEET_FILE_EXTENSION);
		PrintStream fs;
		try {
			fs = new PrintStream(new FileOutputStream(d, false));
			fs.println(ndeDto.getTweet());
			fs.println();
			fs.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	private static Integer getLastIdAdded() {
		return Math.max(getLastIdByClass(DatasetClass.SPAM.getDirValue()), getLastIdByClass(DatasetClass.NO_SPAM.getDirValue()));
	}

	private static Integer getLastIdByClass(String clasif) {
		File dir = new File(DATA_SET_DIR + "/" + clasif);
		File[] files = dir.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				return name.startsWith(TWEET_ADDED_PREFIX);
			}
		});
		return files.length > 0 ? getMaxId(files) : 0;
	}
	
	private static Integer getMaxId(File[] files) {
		   return Stream.of(files).map(DatasetUtils::getIdFromFile).max(Integer::compare).get();
	}

	private static Integer getIdFromFile(File file) {
		return Integer.valueOf(file.getName().replace(TWEET_ADDED_PREFIX, "").replace(TWEET_FILE_EXTENSION, ""));
	}
	
	public static DatasetClass inferClass(String strClass) {
		return strClass.equals("spam") ? DatasetClass.SPAM : DatasetClass.NO_SPAM;
	}

}
