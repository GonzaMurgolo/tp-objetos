package backend;

import static backend.DatasetUtils.generateArff;

public class Context {

	private Clasificador clasificador;

	
	public Context(Clasificador classifier) throws Exception { 
		this.clasificador = classifier;
		clasificador.loadDataset(DatasetUtils.DATA_ARFF);

		boolean exists = clasificador.loadModel(); 

		if (!exists) { 
			trainModel();
		}

	}

	private void trainModel() throws Exception {
		this.clasificador.loadDataset(DatasetUtils.DATA_ARFF);
		this.clasificador.evaluate();
		this.clasificador.config();
		this.clasificador.learn();
		this.clasificador.saveModel();
	}

	public String classify(String tweet) throws Exception {
		clasificador.load(tweet);
		clasificador.makeInstance();
		return clasificador.classify();
	}
	
	public void reTrainModel() throws Exception {
		generateArff();
		trainModel();
	}
	
}
