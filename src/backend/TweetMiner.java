package backend;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.json.simple.parser.ParseException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import twitter4j.JSONObject;
import twitter4j.Paging;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;

public class TweetMiner {

	private ObjectMapper mapper;
	private Twitter twitter;

	public TweetMiner() {
		this.mapper = new ObjectMapper();
		this.twitter = TwitterFactory.getSingleton();
	}

	public Optional<JSONObject> getTweet(String url) {
		String[] split = url.split("/");
		String twid = split[getIdIndex(split)];
		JSONObject response = null;
		try {
			Status status = twitter.showStatus(Long.parseLong(twid));
			if (status == null) {
				System.out.println("STATUS: NULL");
			} else {
				System.out.println("@" + status.getUser().getScreenName() + " - " + status.getText());
			}
			String strJson = mapper.writeValueAsString(status);
			response = mapper.convertValue(strJson, JSONObject.class);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		return Optional.ofNullable(response);
	}

	private int getIdIndex(String[] split) {
		return split.length - 1;
	}

	public List<JSONObject> getTweetsByUser(String username, Integer amountTweets, TwitterFilterDto filter){
		List<Status> statusList = new ArrayList<>();
		Paging paging = new Paging(1, amountTweets);
		try {
			statusList = twitter.getUserTimeline(username, paging);
		} catch (TwitterException e) {
			System.err.println(e.getErrorMessage());
		}
		return statusList.stream().map(status -> mapToStringJson().apply(status))
				.map(strJson -> mapper.convertValue(strJson, JSONObject.class))
				.filter(strTweet -> filterByKeyword(filter, strTweet))
				.filter(strTweet -> filterByDate(filter, strTweet))
				.collect(Collectors.toList());
	}
	
	private boolean filterByKeyword(TwitterFilterDto filter, JSONObject strTweet) {
		return 	filter != null && filter.getKeyword() != null
				? ((String) strTweet.get("text")).contains(filter.getKeyword())
				: true;
	}
	
	private boolean filterByDate(TwitterFilterDto filter, JSONObject strTweet) {
		if (filter != null) {
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Date tweetDate = null;
			try {
				tweetDate = formatter.parse(formatter.format(strTweet.get("createdAt")));
				if (filter.getFromDate() != null && filter.getToDate() != null) {
					return tweetDate.after(filter.getFromDate()) && tweetDate.before(filter.getToDate());
				}
				if (filter.getFromDate() != null) {
					return tweetDate.after(filter.getFromDate());
				}
				return filter.getToDate() != null ? tweetDate.after(filter.getFromDate()) : true;
			} catch (java.text.ParseException e1) {
			}
		}
		return true;
	}

	public List<JSONObject> getFromJSON(String filename) throws IOException, ParseException {
		List<JSONObject> tweetsSaved = new ArrayList<>();
		BufferedReader br = new BufferedReader(new FileReader(filename));
		tweetsSaved.addAll(
				br.lines().map(strJson -> mapper.convertValue(strJson, JSONObject.class)).collect(Collectors.toList()));
		br.close();
		return tweetsSaved;
	}

	public List<JSONObject> mineTimelineTweets(Integer tweetAmountToFetch) throws TwitterException {
		return mineTweets(null, tweetAmountToFetch);
	}

	public List<JSONObject> mineTimelineTweetsWithFilter(Integer tweetAmountToFetch, TwitterFilterDto filter)
			throws TwitterException {
		return mineTweets(filter, tweetAmountToFetch);
	}

	private List<JSONObject> mineTweets(TwitterFilterDto filter, Integer tweetAmountToFetch) throws TwitterException {
		List<JSONObject> tweetsMined = new ArrayList<>();
		String queryStr = (filter != null ? buildQueryFromFilter(filter) : "") + " lang:en +exclude:retweets +exclude:replies";
		Query query = new Query(queryStr);
		QueryResult result;
		Integer restantes = tweetAmountToFetch;
		while (restantes > 0) {
			query.setCount(restantes);
			result = twitter.search(query);
			restantes -= result.getTweets().size();
			query = result.nextQuery();
			System.out.println("Tweets found: " + result.getCount());
			tweetsMined.addAll(result.getTweets().stream().map(status -> mapToStringJson().apply(status))
					.map(strJson -> mapper.convertValue(strJson, JSONObject.class)).collect(Collectors.toList()));
		}
		return tweetsMined;
	}

	private static String buildQueryFromFilter(TwitterFilterDto filter) {
		StringBuilder strBuilder = new StringBuilder();
		Format formatter = new SimpleDateFormat("yyyy-MM-dd");
		strBuilder.append(filter.getKeyword() != null ? filter.getKeyword() : "");
		strBuilder.append(filter.getFromDate() != null ? " since:" + formatter.format(filter.getFromDate()) : "");
		strBuilder.append(filter.getToDate() != null ? " until:" + formatter.format(filter.getToDate()) : "");
		return strBuilder.toString();

	}

	public Function<Status, String> mapToStringJson() {
		return (tweet) -> {
			try {
				return mapper.writeValueAsString(tweet);
			} catch (JsonProcessingException e) {
				throw new RuntimeException();
			}
		};
	}

}
