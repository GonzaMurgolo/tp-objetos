package backend;

import java.io.Serializable;
import java.util.List;

import weka.core.stopwords.StopwordsHandler;


public class StopWordHandlerEng implements StopwordsHandler, Serializable {

	private static final long serialVersionUID = 992607357772009332L;
	private List<String> stopwords;

    public StopWordHandlerEng(List<String> stopwords){
        this.stopwords = stopwords;
    }

    @Override
    public boolean isStopword(String s) {
        return stopwords.contains(s);
    }
}
