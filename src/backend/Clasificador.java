package backend;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintStream;
import java.text.Normalizer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.stemmers.SnowballStemmer;
import weka.core.stemmers.Stemmer;
import weka.core.stopwords.StopwordsHandler;
import weka.filters.unsupervised.attribute.StringToWordVector;

public abstract class Clasificador {
	private static final int NEW_WORDS_TO_KEEP = 1000;
	private static final int FOLDS_AMOUNT = 10;
	private static final int CANT_VALORES_CLASES = 2;

	protected Instances trainData;
	protected StringToWordVector filter;
	protected FilteredClassifier classifier;
	protected Instances instances;
	protected String textToClassify;
	protected Instances train;

	public Clasificador() {
	}

	protected void config() {
		try {
			StringBuilder log = new StringBuilder();

			classifier = new FilteredClassifier();
			classifier.setFilter(filter);
			classifier.setClassifier(getClassifier());
			log.append(getClassifierName());
			Evaluation eval = new Evaluation(trainData);
			eval.crossValidateModel(classifier, trainData, FOLDS_AMOUNT, new Random(1));
			log.append(eval.toSummaryString());
			log.append(eval.toClassDetailsString());
			log.append(eval.toMatrixString());

			saveLog(log);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveModel() {
		try {
			ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(getClassifierName()));
			out.writeObject(classifier);
			out.close();
			System.out.println("===== Saved model: " + getClassifierName() + " =====");
		} catch (IOException e) {
			System.out.println("Problem found when writing: " + getClassifierName());
		}
	}
	
	public boolean loadModel() {
		try (ObjectInputStream  in = new ObjectInputStream(new FileInputStream(getClassifierName()))) {
			Object tmp = in.readObject();
			classifier = (FilteredClassifier) tmp;
			
			System.out.println("===== Loaded model: " + getClassifierName() + " =====");
			return true;
		} catch (Exception e) {
			System.out.println("Problem found when reading: " + getClassifierName());
			return false;
		}
	}

	public abstract String getClassifierName();
	
	public abstract Classifier getClassifier();

	public void saveLog(StringBuilder data) throws FileNotFoundException {
		StringBuilder dateString = new StringBuilder().append("_");
		dateString.append(DateTimeFormatter.ofPattern("dd-MM-yyyy").format(LocalDateTime.now()));
		String path = new String("Log");
		File directorio = new File(path);
		directorio.mkdir();
		File d = new File(path + "\\" + getClassifierName() + dateString + ".txt");
		PrintStream fs = new PrintStream(new FileOutputStream(d, false));
		fs.println(data);
		fs.close();
	}

	public void loadDataset(String fileName) {
		try {
			BufferedReader reader = new BufferedReader(new FileReader(fileName));
			ArffLoader.ArffReader arff = new ArffLoader.ArffReader(reader);
			trainData = arff.getData();
			trainData.setClassIndex(DatasetUtils.CLASS_INDEX);

			System.out.println("===== Loaded dataset: " + fileName + " =====");
			reader.close();
		} catch (IOException e) {
			System.out.println("Problem found when reading: " + fileName);
		}
	} 

	public List<String> loadStopwords(String filename) {
		ArrayList<String> stopwords = new ArrayList<>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(filename));
			String line;
			while ((line = reader.readLine()) != null) {
				stopwords.add(line);
			}
			reader.close();
		} catch (IOException e) {
			System.out.println("No se pudo cargar el archivo: " + filename);
		}
		return stopwords;
	} 

	public void evaluate() {
		try {
			filter = new StringToWordVector();
			filter.setInputFormat(trainData);
			filter.setIDFTransform(true);
			filter.setLowerCaseTokens(true);
			filter.setWordsToKeep(NEW_WORDS_TO_KEEP);

			List<String> stw = loadStopwords("NecesaryFiles/stop_words.txt");
			StopwordsHandler swh = new StopWordHandlerEng(stw);
			filter.setStopwordsHandler(swh);

			SnowballStemmer ste = new SnowballStemmer();
			ste.setStemmer("english");
			filter.setStemmer(ste);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void learn() throws Exception {
		classifier.buildClassifier(trainData);
	}

	public void load(String textToClassify) {
		this.textToClassify = textToClassify;
	}

	public String classify() {
		try {
			double pred = classifier.classifyInstance(instances.instance(0));
			double[] distribution = classifier.distributionForInstance(instances.instance(0));
			return instances.classAttribute().value((int) pred);
		} catch (Exception e) {
			System.out.println("Problem found when classifying the text");
			return "null";
		}
	}

	public String procesaTexto(String s) {
		String cadenaNormalize = Normalizer.normalize(s, Normalizer.Form.NFD);
		s = cadenaNormalize.replaceAll("[^\\p{ASCII}]", "");
		s = s.replaceAll("\\p{Punct}", "");
		String sl = s.replaceAll("\\d", "");
		sl = sl.toLowerCase();
		return sl;
	}

	public void makeInstance() {
		ArrayList<String> valoresClase = new ArrayList<>(CANT_VALORES_CLASES);
		valoresClase.add("no-spam");
		valoresClase.add("spam");
		Attribute attribute1 = new Attribute("text", (List<String>) null);
		Attribute attribute2 = new Attribute("@class", valoresClase);
		ArrayList<Attribute> esquemaDataset = new ArrayList<>(3);
		esquemaDataset.add(0, attribute1);
		esquemaDataset.add(1, attribute2);
		instances = new Instances("Test relation", esquemaDataset, 1);
		instances.setClassIndex(DatasetUtils.CLASS_INDEX);
		DenseInstance instance = new DenseInstance(2);
		instance.setDataset(instances);
		textToClassify = procesaTexto(textToClassify);

		instance.setValue(0, textToClassify);
		instance.setClassMissing();

		instances.add(instance);
	}
}
