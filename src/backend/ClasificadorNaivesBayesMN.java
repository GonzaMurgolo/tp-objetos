package backend;

import weka.classifiers.Classifier;
import weka.classifiers.bayes.NaiveBayesMultinomial;

public class ClasificadorNaivesBayesMN extends Clasificador {

	private static final String CLASSIFIER_NAME = "NaiveBayesMultinomial";

	@Override
	public String getClassifierName() {
		return CLASSIFIER_NAME;
	}


	@Override
	public Classifier getClassifier() {
		return new NaiveBayesMultinomial();
	}

}
