package backend;

import weka.classifiers.Classifier;
import weka.classifiers.trees.J48;


public class ClasificadorJ48 extends Clasificador{

    private static final String FILENAME = "J48";

	@Override
	public String getClassifierName() {
		return FILENAME;
	}

	@Override
	public Classifier getClassifier() {
		return new J48();
	}

}