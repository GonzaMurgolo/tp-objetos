package backend;

public enum DatasetClass {
	SPAM("spam"),
	NO_SPAM("no-spam");
	
	private String dirName;
	
	private DatasetClass(String dirName){
		this.dirName = dirName;
	}
	
	public String getDirValue() {
		return dirName;
	}
}
