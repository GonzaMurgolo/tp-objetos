package backend;

public class NewDatasetElementDto {

	private DatasetClass classification;
	
	private String tweet;
	
	public NewDatasetElementDto(DatasetClass classification, String tweet) {
		this.classification = classification;
		this.tweet = tweet;
	}

	public DatasetClass getClassification() {
		return classification;
	}

	public void setClassification(DatasetClass classification) {
		this.classification = classification;
	}

	public String getTweet() {
		return tweet;
	}

	public void setTweet(String tweet) {
		this.tweet = tweet;
	}
	
	
}
