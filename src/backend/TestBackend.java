package backend;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import twitter4j.JSONObject;
import twitter4j.TwitterException;

public class TestBackend {

	private static final int USER_AMOUNT_TWEETS = 5;
	private static final String KEYWORD= "COVID";
	private TweetMiner tM = new TweetMiner();

	public static void main(String[] args) throws TwitterException {
//		DatasetUtils.generateArff();
		TestBackend testBackend = new TestBackend();
//		List<JSONObject> timelineTweets = testBackend.testReadTweets();
//		testBackend.processTweets(timelineTweets);
		testBackend.fetchTweetsFromUser("gonzamurgolo", USER_AMOUNT_TWEETS);
		
	}

	private void processTweets(List<JSONObject> tweets) {
		tweets.stream().map(tw->(String) tw.get("text")).forEach(this::testJ48);
	}
	
	private Optional<String> testJ48(String tweet){
		ClasificadorJ48 classifierJ48 = new ClasificadorJ48();
		String response= null;
		try {
			Context context = new Context(classifierJ48);
			response = context.classify(tweet);
			System.out.println(tweet + " \n Classified as: " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(response);
	}
	
	private Optional<String> testNB(String tweet){
		ClasificadorNaivesBayesMN classifierNB = new ClasificadorNaivesBayesMN();
		String response= null;
		try {
			Context context = new Context(classifierNB);
			response = context.classify(tweet);
			System.out.println(tweet + " \n Classified as: " + response);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Optional.ofNullable(response);
	}
	
	private List<JSONObject> testReadTweets() {
		int cantTwits = 10;
		ArrayList<JSONObject> response = new ArrayList<>();
		try {
			response.addAll(readTweets(cantTwits));
		} catch (TwitterException e) {
			e.printStackTrace();
		}
		return response;
	}

	private List<JSONObject> readTweets(Integer cantTweets) throws TwitterException {
		Integer cant = cantTweets;
		ArrayList<JSONObject> response = new ArrayList<>();
		if (cant > 0) {
//			tM.mineTimelineTweets();
			TwitterFilterDto filter = new TwitterFilterDto();
			filter.setKeyword(KEYWORD);
			try {
				filter.setFromDate(getDate());
			} catch (ParseException e) {
			}
			List<JSONObject> tweets = tM.mineTimelineTweetsWithFilter(cant, filter);;
			logResult(tweets);
			response.addAll(tweets);
		}
		return response;
	}
	
	private Date getDate() throws ParseException {
		String str_date = "2020-8-4";
		DateFormat formatter;
		formatter = new SimpleDateFormat("yyyy-MM-dd");
		return formatter.parse(str_date);	
	}

	private void fetchTweetsFromUser(String username, Integer cantTweets) throws TwitterException {
		if (cantTweets > 0) {
			TwitterFilterDto filter = new TwitterFilterDto();
			filter.setKeyword("trabajar");
			List<JSONObject> tweets = tM.getTweetsByUser(username, cantTweets, filter);
			logResult(tweets);
		}
	}

	private void logResult(List<JSONObject> tweets) {
		System.out.println("Tweets fetched :" + tweets.size());
		tweets.forEach(tw -> System.out.println(tw.get("text").toString()));
	}
}
